import collections as c


def deque():
    q = c.deque()
    q.append(1)
    q.append(2)
    q.append(4)
    q.append(3)
    print(q)


deque()


def ordered_dict():
    d = c.OrderedDict()
    d['a'] = 1
    d['b'] = 2
    print(d)


ordered_dict()
