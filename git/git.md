# git学习

##  一、Git超级入门

### Git 全局设置（ 按照下面格式修改邮箱，每台电脑都要配置 ）

```
  # 修改工号和名字
  git config --global user.name "name"
  git config --global user.email "name@163.com"
  
  # windows支持长路径，要不然代码路径太长会报错
  git config --global core.longpaths true
  
  # windows默认下载git代码，没有显示链接link属性问题
  git config --global core.symlinks true
  
  # windows下载代码编码格式，不会进行转换，提交不转换（默认安装会转换成windows）
  git config --global core.autocrlf false
```
### 配置Git SSH KEY（ 按照下面格式修改邮箱 ）
```
ssh key有什么用：每台电脑（终端）的公钥，用来和git平台校验，这样子每次提交的时候无需输入用户名和密码

#生成ssh key
ssh-keygen -t rsa -C "name@163.com" -b 4096
#然后一直回车

cat ~/.ssh/id_rsa.pub
#把打印的公钥字符串复制，配置到git平台上面

```

### 最简单开发过程（步骤）

`#1、下载代码（以code命名仓库为例）`

```
git clone git@gitlab.com:eastonyangxu/python-study-master.git
cd python-study-master
```

`#2、查看所有分支`

```
git branch -av
```

`#3、切换已经存在的分支`
```
git checkout  xxx
git branch  #再次查看分支
```

`#4、从master新建分支，并且切换到新分支（加上-b选项）`
```
git checkout -b feature-wdw-login master
git branch  #再次查看分支
```


`#5、在个人分支增删改代码，按照平常编写代码`
```
  撸呀撸代码
  调试代码
  编写单测
  终于调通准备提交代码，往下看

```

`#6、提交代码分4步`

```
#查看有那些文件是被你修改，以防修改不必要文件（有改动文件，显示红色）
git status    

#添加准备要提交的代码到暂存区（暂存区概念学习git时候查一下。 有改动文件，这时候显示绿色）
git add xxx.c
#再次查看添加了什么，确保正确
git status

#提交代码到本地仓库（注意这个时候还未上传代码到git服务器）
git commit -m "[ADD]: 提交新代码xxx.c"   

 #真正提交代码到远程仓库，这里提交到个人分支
git push origin feature-wdw-login  
```

## 二、代码冲突几种场景处理
-   **1、 提交合并请求，发现平台提示冲突 （有冲突无法合并，直接修改冲突，再次提交即可，无需在提交合并请求）**

    `1) 直接在平台合并请求页面修改 -- 点击 "解决冲突" 按钮，直接在平台修改冲突代码（适合很小的冲突）`  
    `2) 在本地解决冲突，见下面第二种方法`
    
-   **2、 提交代码前，想更新master代码（merge）到个人分支方便本地测试，平常没有此需求不需要这么做 （ 此时如有冲突会提示 ）**  
    `git checkout master`   （切换到master分支）  
    `git pull origin master`    （ 更新本地电脑的master最新代码 ）   
    `git checkout 个人分支` （ 切回到个人分支 ）  
    `git merge --ff master` ( merge操作，把本地的master最新代码，合并到个人分支，可能会有冲突，这时候master其他人的提交记录，也会合并在个人分支 )  
    如有冲突，如： CONFLICT (content): Merge conflict xxx   
    【这种场景代码冲突，解决方法如下】  
    `1) 手工解决冲突，直接在编辑器修改代码，手工合并代码`  
    `2) git status` （查看代码变更状态，以防改错其他文件）  
    `3) git add 冲突文件`  
    `4) git commit -m "log xxx"` （提交代码到本地）  

-   **3、 一个分支多个人一起开发，在提交代码前先更新分支代码，发现有冲突**  
    `git checkout 个人分支` （ 切回到个人分支 ） 
    `编写分支代码、提交到本地分支、然后更新分支库上最新代码`  
    `git pull origin 个人分支` （ 更新个人分支最新代码，把其他人员提交代码更新，可能会有冲突 ）  
    如有冲突，如： CONFLICT (content): Merge conflict xxx   
    【这种场景代码冲突，解决方法如下】  
    `1) 手工解决冲突，直接在编辑器修改代码，手工合并代码`  
    `2) git status` （查看代码变更状态，以防改错其他文件）  
    `3) git add 冲突文件`  
    `4) git commit -m "log xxx"` （提交代码到本地）  

## 三、术语说明
```
【流水线 CI/CD】
- 流水线说的就是持续集成（CI），以前svn用的是jenkins
- 流水线每个节点，就是一个任务（job），每个任务都是不同的功能： 如代码扫描job、单元测试job、打包job
```
```
【保护分支：】
- 受保护的分支: `master` ，功能分支全部合并到`master`分支  
- 受保护分支只有`主程序员`才能有权限合并代码  
```
```
【合并请求：】
- 有了保护的分支概念，就有合并请求，因为普通开发没有权限合入代码，只能通过合并请求申请
- 指派人： 指派就是主程序员，普通开发人员提交合并请求时候，需要指派的人(需要提交合并请求开发填写)
- @干系人： 干系人 意思就是这次提交的代码也需要“某干系人” 去帮我review代码； 一般是模块有关联人员
```
```
【权限说明：】
- 主程序员权限： 有保护分支合并权限的人，一般是项目经理、技术指导经理  
- 开发人员权限： 普通开发人员，无法直接修改master代码，只能新建分支，通过合并请求申请合入  
- 报告者权限： 只能看代码权限，无法提交（包括个人分支）
```

## 四、命令

### branch

```
查看本地分支：git branch
查看远程分支：git branch -r
查看本机及远程的分支：git branch -a
创建新分支：git branch <new-branch-name>
更改分支名(-m换成-M,表示强制)：git branch -m <old-branch-name> <new-branch-name>
删除本地分支(-d换成-D,表示强制)：git branch -d <branch-name>
本地分支关联远程分支：git branch --set-upstream-to origin/<branch-name>
```

### checkout

```
基于当前分支新建分支并切换到新分支：git checkout -b <new-branch-name>
基于远程某个分支新建并切换到新分支：git checkout -b <remote-branch-name> origin/<remote-branch-name>
使用上面命令失败，拉不下代码的情况：git remote show origin、git remote update、git fetch 后再使用命令
切换到某个分支：git checkout <branch-name>
git checkout -b <branch-name> = git branch <branch-name> + git checkout <branch-name>
放弃工作区中所有的更改：git checkout .
放弃工作区中某个文件的更改：git checkout -- file-name
强制放弃 index 和 工作区 的改动：git checkout -f
```

### push

```
本地分支推送到远程：git push origin <branch-name>
强制推送到远程：git push -f origin <branch-name>
删除远程分支：git push --delete origin <branch-name>
推送并关联远程分支：git push --set-upstream origin <branch-name>
```

### tag

```
为当前分支所在的提交记录打上轻量标签：git tag <lightweght-name>
为某次具体的提交记录打上轻量标签：git tag <lightweght-name> <commit SHA-1 value>
为当前分支所在的提交记录打上附注标签：git tag -a <anotated-name> -m <tag-message>
列出所有的标签名：git tag
删除某个标签，本质上就是移除 .git/refs/tags/ 中对应的文件：git tag -d <tag-name>
显示标签对应提交记录的具体信息：git show <tag-name>
推送某个标签到远程仓库：git push origin <tag-name>
推送所有标签到远程仓库：git push origin --tags
删除远程仓库中的某个标签：git push --delete origin <tag-name>
基于标签建立分支：git branch <new-branch> <tag-name>
```

### reset

```
--mixed(默认)：恢复到工作区，add前
--soft：恢复到暂存区（commit前，add后）
--merge：
--hard：放弃
--keep：恢复到暂存区（commit前，add后），并且保留当前修改

放弃修改，回退到上次提交：git reset --hard
恢复到工作区，add前：git reset --mixed HEAD^
回滚到指定提交commit前add后：git reset --soft <commit-id>
回滚到上上个版本，并且保留工作区的修改：git reset --keep HEAD~2
将当前分支与远程分支状态保持一致：git reset --hard origin/<branch-name>
```

### rm

```
将文件从暂存区和工作区中删除：git rm <file-name>
如果文件有修改，强制删除：git rm -f <file-name>
将文件从暂存区移除：git rm --cached <file-name>
递归删除目录下的所有文件：git rm -r <dir-name>
将所有文件从暂存区移除：git rm -r --cached .
```

### stash

```
使用场景：在当前分支branch1修改了代码，但是临时有个问题需要切换到branch2修复紧急bug，这时就可以先git add .（把所有改动加入到暂存区） 然后执行 git stash save '<stash-msg>' (把改动加入到缓存区)，然后切换到branch2修改bug，修改完bug以后切回到branch1，然后执行git stash pop （把改动恢复到工作区），然后继续工作即可。

查看缓存区列表：git stash list
查看缓存详细信息，查看最近一条记录：git stash show    查看指定(索引为2)：git stash show stash@{2}
把暂存区代码加入到缓存区：git stash save '<stash-msg>'
删除最后一条记录：git stash drop    删除指定(索引为1)：git stash drop stash@{1}
把暂存区代码恢复到工作区，并且删除暂存区该条记录，恢复最后一条：git stash pop
恢复指定一条（索引为2）,并且删除缓存区该条记录：git stash pop stash@{2}
恢复但不删除最后一条记录：git stash apply     恢复但不删除指定一条(索引为2)：git stash apply stash@{2}
清空缓存区所有记录：git stash clear
```

### rebase

```
合并n次提交为一个提交：git rebase -i HEAD~n
```

### 其他命令

```
克隆远程仓库到本地：git clone <url>
初始化本地仓库：git init
查看本地工作区状态：git status
所有文件加入到暂存区：git add .
提交代码：git commit -m '<commit-msg>'
本次提交合入到上次提交中，本次提交会改变commit-id：git commit --amend -m '<commit-msg>'
更新远程分支信息到本地：git fetch
更新远程某一个分支信息到本地：git fetch origin <branch-name>
需要查看某个命令的文档，例如查看reset的用法：git reset -h
拉取某一个提交到本分支：git cherry-pick <commit-id>
合并其他分支到本分支：git merge <branch-name>
撤销工作区代码：git restore .
查看修改的代码：git diff
查看提交记录：git log
查看详细的记录：git reflog
配置相关：git config  (详细使用方式进行百度，一般使用参考该文档第一节的全局设置)
```

## 五、.gitignore文件

```
忽略该文件下配置的文件
```

